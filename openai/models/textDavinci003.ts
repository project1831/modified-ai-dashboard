import { ChatInteraction } from '../../types'
import Mock from '../Mock/mock_1_GPT.json'
import MockIncomplete from '../Mock/inComplete_mock.json'
import swal from 'sweetalert'
import Swal from 'sweetalert2'
import { OPEN_AI_COMPLETIONS_DAVINCI_API } from '../../config'
function isValidJson(str: string) {
  try {
    JSON.parse(str)
    return true
  } catch (e) {
    return false
  }
}

const makeValidJSON = async (inValidJson: any, settings: any) => {
  Swal.fire({
    title: 'Further Cleansing the output response',
    text: 'Do not close the tab. Please wait...',
    icon: 'info',
    timerProgressBar: true,
    didOpen: () => {
      Swal.showLoading() // This displays the loader
    },
  })

  const response = await fetch(settings.url, {
    headers: {
      'api-key': settings.apikey,
      'Content-Type': 'application/json',
      authorization: `Bearer ${settings.auth}`,
    },
    method: 'POST',
    body: JSON.stringify({
      max_tokens: 2000,
      stop: ['Human:', 'AI:'],
      temperature: 0.3,
      messages: [
        {
          role: 'system',
          content: 'AI',
        },
        {
          role: 'user',
          content: ` Correct the Input JSON and return a valid JSON data, Remove incomplete function also Remove incomplete json object item

            Input JSON: ${inValidJson}

            Instructions::
            1) Remove incomplete function 
            2) Remove incomplete json object item

            The following topics should be considered:
            - Reply only in JSON format
            - without any whitespace characters such as "The corrected JSON data is:"
            
          `,
        },
      ],
    }),
  })
    .then((response) => response.json())
    .then((resp) => {
      console.log('Ultimate response=======>', resp.choices[0].message.content)
      Swal.close()
      return resp
    })

  return response
}

export async function queryTextDavinci003Completions(
  prompt: string,
  options: { apikey: string; model: string },
): Promise<ChatInteraction[] | undefined> {
  const gptData = localStorage.getItem('gpt-data')
  const analyzer_settings = localStorage.getItem('analyzer-settings')

  if (analyzer_settings) {
    const settings = JSON.parse(analyzer_settings)
    if (settings?.mock === 'true') {
      console.log('Mock data')

      const chat = [
        {
          reply: Mock.choices?.[0]?.text || '',
        },
      ]
      localStorage.setItem('gpt-data', JSON.stringify(chat))
      return chat
    } else {
      if (gptData) {
        console.log('Cashes data')
        return JSON.parse(gptData)
      } else {
        return fetch(settings.url, {
          headers: {
            'api-key': settings.apikey,
            'Content-Type': 'application/json',
            authorization: `Bearer ${settings.auth}`,
          },
          method: 'POST',
          body: JSON.stringify({
            max_tokens: 2000,
            stop: ['Human:', 'AI:'],
            temperature: 0.3,
            messages: [
              {
                role: 'system',
                content: 'AI',
              },
              {
                role: 'user',
                content: prompt,
              },
            ],
          }),
        })
          .then((response) => response.json())
          .then(async (resp) => {
            // console.log('resp=========>', resp.choices[0].message.content)

            if (resp?.error) {
              swal(
                'Warning!',
                resp?.error.message,
                'warning',
              ).then((value) => {})
              return
            }

            if (!isValidJson(resp.choices[0].message.content)) {
              const validJSON: any = await makeValidJSON(
                resp.choices[0].message.content,
                settings,
              )
              const chat = [
                {
                  reply: validJSON.choices[0].message.content || '',
                },
              ]
              localStorage.setItem('gpt-data', JSON.stringify(chat))
              return chat
            }

            const chat = [
              {
                reply: resp.choices[0].message.content || '',
              },
            ]
            localStorage.setItem('gpt-data', JSON.stringify(chat))
            return chat
          })
          .catch((resp) => {
            // console.log(resp)
            swal('Warning!', resp?.error.message, 'warning').then((value) => {})
            const chat = [
              {
                reply: Mock.choices?.[0]?.text || '',
              },
            ]
            return chat
          })
      }
    }
  } else {
    return undefined
  }
}
