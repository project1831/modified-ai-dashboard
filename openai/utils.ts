import { OPEN_AI_COMPLETIONS_API, AZURE_OPEN_AI_API } from '../config'

import Mock from './Mock/mock_1_GPT.json'
import MockIncomplete from './Mock/inComplete_mock.json'
import swal from 'sweetalert'

function isValidJson(str: string) {
  try {
    JSON.parse(str)
    return true
  } catch (e) {
    return false
  }
}

export function fetchOpenAiGPT3Turbo(
  options: any,
  prompt: string,
  temperature?: number,
) {
  //   return fetch(OPEN_AI_COMPLETIONS_API, {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       authorization: `Bearer ${apikey}`,
  //     },
  //     method: 'POST',
  //     body: JSON.stringify({
  //       max_tokens: 2000,
  //       model: 'gpt-3.5-turbo',
  //       temperature: 0.3,
  //       messages: [
  //         { role: 'system', content: prompt.split('Human:')[0] },
  //         { role: 'user', content: prompt.split('Human:')[1] },
  //       ],
  //     }),
  //   })
  //     .then((response) => response.json())
  //     .then((resp) => {
  //       const chat = [
  //         {
  //           reply: resp.choices?.[0]?.message?.content || '',
  //         },
  //       ]
  //       return chat
  //     })

  var myHeaders = new Headers()
  myHeaders.append('Content-Type', 'application/json')
  myHeaders.append('api-key', options.apikey)
  myHeaders.append(
    'Authorization',
    'Bearer Bearer a8fe99cb6b354a06913f189536cdf8fc',
  )

  var raw = JSON.stringify({
    messages: [
      { role: 'system', content: prompt.split('Human:')[0] },
      { role: 'user', content: prompt.split('Human:')[1] },
    ],
    // max_tokens: 800,
    temperature: temperature,
    // frequency_penalty: 0,
    // presence_penalty: 0,
    // top_p: 0.95,
    // stop: null,
  })

  var requestOptions: any = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  }

  return fetch(AZURE_OPEN_AI_API, requestOptions)
    .then((response) => response.json())
    .then((resp) => {
      if (resp?.error) {
        swal('Warning!', resp?.error.message, 'warning').then((value) => {})
        return
      }

      if (!isValidJson(resp.choices?.[0]?.text)) {
        swal(
          'Warning!',
          'Somthing wrong with the server, please Re-Analyze',
          'warning',
        ).then((value) => {})
        return
      }

      const chat = [
        {
          reply: resp.choices?.[0]?.message?.content || '',
        },
      ]
      console.log('====>', resp)
      localStorage.setItem('gpt-data', JSON.stringify(chat))

      return chat
    })
    .catch((error) => console.log('error', error))
}
