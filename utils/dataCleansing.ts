import AutoDeleteIcon from '@mui/icons-material/AutoDelete'
import PublishedWithChangesIcon from '@mui/icons-material/PublishedWithChanges'

export function sanitizeArray(data: any) {
  let logs = []
  // 1) Remove columns that have more than 5 missing data
  let missingCounts: any = {}

  data.forEach((entry: any) => {
    for (let key in entry) {
      if (!entry[key]) {
        missingCounts[key] = (missingCounts[key] || 0) + 1
      }
    }
  })

  let columnsToRemove: any = []
  for (let key in missingCounts) {
    if (missingCounts[key] > 5) {
      columnsToRemove.push(key)
      logs.push({
        column: key,
        reason: `Will not consider due to more than 5 missing data.`,
        Icon: AutoDeleteIcon,
        color: '#E74C3C',
        type: 'remove',
      })
    }
  }

  let processedData = data.map((entry: any) => {
    let newEntry = { ...entry }
    columnsToRemove.forEach((col: any) => {
      delete newEntry[col]
    })
    return newEntry
  })

  // 2) Convert numeric string to number
  processedData = processedData.map((entry: any) => {
    let newEntry: any = {}
    for (let key in entry) {
      if (
        key !== 'Date' &&
        !isNaN(parseFloat(entry[key])) &&
        isFinite(entry[key])
      ) {
        newEntry[key] = parseFloat(entry[key])
        if (entry[key] !== newEntry[key].toString()) {
          logs.push({
            column: key,
            reason: `Converted field "${key}" from string "${entry[key]}" to number ${newEntry[key]}.`,
            Icon: PublishedWithChangesIcon,
            color: '#27AE60',
            type: 'convert',
          })
        }
      } else {
        newEntry[key] = entry[key]
      }
    }
    return newEntry
  })

  // Log on a different array
  let resultArray: any = []
  processedData.forEach((entry: any) => {
    resultArray.push(entry)
  })

  return { resultArray, logs }
}
