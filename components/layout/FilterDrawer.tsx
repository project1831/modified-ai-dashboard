import * as React from "react";
import Divider from "@mui/material/Divider";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import Fab from "@mui/material/Fab";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import TuneIcon from "@mui/icons-material/Tune";
type Anchor = "top" | "left" | "bottom" | "right";

type Props = {
  FilterUI: any;
};

export default function SwipeableTemporaryDrawer({ FilterUI }: Props) {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  return (
    <div>
      <React.Fragment key={1}>
        <Fab
          sx={{
            position: "fixed",
            bottom: 16,
            right: 16,
          }}
          color="primary"
          aria-label="add"
          onClick={toggleDrawer("right", true)}
        >
          <FilterAltIcon />
        </Fab>
        <SwipeableDrawer
          anchor={"right"}
          open={state["right"]}
          onClose={toggleDrawer("right", false)}
          onOpen={toggleDrawer("right", true)}
        >
          <div key={1} style={{ display: "block" }}>
            <div style={{ display: "flex", padding: "2rem" }}>
              <TuneIcon style={{ color: "#0282ad" }} fontSize="large" />
              <b className="nav-text-head">Apply Filter </b>
            </div>
          </div>
          <Divider style={{ marginTop: 15, marginBottom: 25 }} />
          <FilterUI />
        </SwipeableDrawer>
      </React.Fragment>
    </div>
  );
}
