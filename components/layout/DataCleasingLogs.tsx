import * as React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import ImageIcon from "@mui/icons-material/Image";
import WorkIcon from "@mui/icons-material/Work";
import BeachAccessIcon from "@mui/icons-material/BeachAccess";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

export default function FolderList({ logs }: any) {
  return (
    <>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Modified columns and data</Typography>
        </AccordionSummary>
        <AccordionDetails style={{ display: "flex" }}>
          <List
            sx={{ width: "100%", bgcolor: "background.paper" }}
            style={{ height: 400, overflowY: "auto" }}
          >
            <h4>Removed Columns</h4>
            {logs
              .filter((val: any) => val.type === "remove")
              .map((val: any, i: number) => (
                <ListItem key={i}>
                  <ListItemAvatar>
                    <Avatar style={{ background: `${val.color}` }}>
                      {<val.Icon />}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={val.column} secondary={val.reason} />
                </ListItem>
              ))}
          </List>
          <List
            sx={{ width: "100%", bgcolor: "background.paper" }}
            style={{ height: 400, overflowY: "auto" }}
          >
            <h4>Converted Data</h4>
            {logs
              .filter((val: any) => val.type === "convert")
              .map((val: any, i: number) => (
                <ListItem key={i}>
                  <ListItemAvatar>
                    <Avatar style={{ background: `${val.color}` }}>
                      {<val.Icon />}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={val.column} secondary={val.reason} />
                </ListItem>
              ))}
          </List>
        </AccordionDetails>
      </Accordion>
    </>
  );
}
