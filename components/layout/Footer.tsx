import React from "react";

function FullWidthFooter() {
  return (
    <footer className="footer">
      <p>&copy; 2023 Your Company. All rights reserved.</p>
    </footer>
  );
}

export default FullWidthFooter;
