import React from "react";
import styles from "../../styles/Components.module.scss";
import { Button } from "./Button";
import { ButtonLink } from "./ButtonLink";
import ManageHistoryIcon from "@mui/icons-material/ManageHistory";

export function DataLoadedMessage(
  props: React.PropsWithChildren<{
    onAnalyze: () => void;
  }>
) {
  return (
    <div className={styles.emptyMessageContainer}>
      <div className={styles.emptyMessage}>
        <Button className="analyze" onClick={props.onAnalyze}>
          <ManageHistoryIcon style={{ marginRight: 10 }} /> Re-AnalySe
        </Button>
      </div>
    </div>
  );
}
