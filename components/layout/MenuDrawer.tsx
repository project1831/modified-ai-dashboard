import * as React from "react";
import Image from "next/image";
import logoautograph from "../../images/logo-autograph.png";
import Box from "@mui/material/Box";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import Fab from "@mui/material/Fab";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import Button from "@mui/material/Button";
import MenuIcon from "@mui/icons-material/Menu";
import WidgetsIcon from "@mui/icons-material/Widgets";

type Anchor = "top" | "left" | "bottom" | "right";

type Props = {
  FilterUI: any;
};

export default function SwipeableTemporaryDrawer({ FilterUI }: Props) {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
      (event: React.KeyboardEvent | React.MouseEvent) => {
        if (
          event &&
          event.type === "keydown" &&
          ((event as React.KeyboardEvent).key === "Tab" ||
            (event as React.KeyboardEvent).key === "Shift")
        ) {
          return;
        }

        setState({ ...state, [anchor]: open });
      };

  return (
    <div>
      <React.Fragment key={1}>
        {/* <Button
          variant="outlined"
          
          style={{ color: "white" }}
        >
   
        </Button> */}
        <div onClick={toggleDrawer("left", true)} style={{ display: "flex" }}>
          <div className="ag-header">
          <div className="ag-logo">
            <span className="new-logo">
              <Image src={logoautograph} alt="" />
            </span>
            <b className="nav-text-head">Auto-Graph</b>
          </div>
          </div>
        </div>
        <SwipeableDrawer
          anchor={"left"}
          open={state["left"]}
          onClose={toggleDrawer("left", false)}
          onOpen={toggleDrawer("left", true)}
          style={{ background: "" }}
        >
          <FilterUI />
        </SwipeableDrawer>
      </React.Fragment>
    </div>
  );
}
