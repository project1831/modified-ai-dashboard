import * as React from "react";
import loader from "../../Asset/strategy-main.gif";
import Image from "next/image";

const GraphLoader = () => {
  return (
    <div>
      <div className="center-button-wrapper" style={{ background: "white" }}>
        <center>
          <Image src={loader} alt="ii" style={{ height: 300, width: "100%" }} />
          <br />
          <br />
          <p>Generating graph...</p>
        </center>
      </div>
    </div>
  );
};

export default GraphLoader;
