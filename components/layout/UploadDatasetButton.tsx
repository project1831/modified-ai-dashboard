import React from "react";
import { Button } from "./Button";
import styles from "../../styles/Components.module.scss";
import { parseData, stringifyData } from "../../utils/parseData";
import { useCoreData } from "../../pages/_app";
import { parseISO, format } from "date-fns";

import { sanitizeArray } from "../../utils/dataCleansing";
interface UploadDatasetButtonProps {
  onUpload: (dataset: string) => void;
  resetContext: any;
  setLogs: any;
}

export function UploadDatasetButton(props: UploadDatasetButtonProps) {
  let { coreData, setCoreData } = useCoreData();

  const inputFileRef = React.useRef<HTMLInputElement>(null);
  const handleUploadFileClick = React.useCallback(() => {
    inputFileRef.current?.click?.();
  }, []);
  const [settings, setSettings] = React.useState<any>("");
  const handleDownload = () => {
    const filePath = "data.csv"; // Specify the path to your sample CSV file

    // Create a temporary anchor element
    const link = document.createElement("a");
    link.href = filePath;
    link.download = "sample.csv"; // Specify the desired filename

    // Simulate a click event to trigger the download
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  React.useEffect(() => {
    const getData = localStorage.getItem("analyzer-settings");
    const settingsData = getData && JSON.parse(getData);
    setSettings(settingsData);
  }, []);

  function makeArrayUniform(arr: any) {
    if (!Array.isArray(arr) || arr.length === 0) {
      throw new Error("Input should be a non-empty array.");
    }

    // Get all unique keys present in the array
    const allKeys = arr.reduce((keys, item) => {
      Object.keys(item).forEach((key) => {
        if (!keys.includes(key)) {
          keys.push(key);
        }
      });
      return keys;
    }, []);

    // Filter out elements that are not uniform (do not have all keys)
    const uniformArray = arr.filter((item) => {
      const itemKeys = Object.keys(item);
      return (
        itemKeys.length === allKeys.length &&
        itemKeys.every((key) => allKeys.includes(key))
      );
    });

    // Fill missing keys with null in each object of the array
    const filledArray = uniformArray.map((item) => {
      const newItem = { ...item };
      allKeys.forEach((key: any) => {
        if (!newItem.hasOwnProperty(key)) {
          newItem[key] = null;
        }
      });
      return newItem;
    });

    return filledArray;
  }

  function isDateString(obj: any, xKey: any) {
    if (typeof obj[xKey] !== "string") {
      return false;
    }

    return !isNaN(Date.parse(obj[xKey]));
  }

  const handleUploadFile = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault();

      props.resetContext();
      if (e.target) {
        const inputFile = (e.target as HTMLInputElement).files?.[0];
        const reader = new FileReader();

        reader.onload = function (event) {
          const text = event?.target?.result as string;
          if (text) {
            const data = parseData(text);

            // console.log("======data======>", data);
            console.log("============>", sanitizeArray(data));

            // eslint-disable-next-line react-hooks/exhaustive-deps
            const sanatizeData = sanitizeArray(data);
            setCoreData(sanatizeData?.resultArray);
            // setCoreData(data);
            props.setLogs(sanatizeData?.logs);

            props.onUpload?.(stringifyData(sanatizeData?.resultArray));
          }
        };

        if (inputFile) reader.readAsText(inputFile);
      }
    },
    [props.onUpload]
  );

  return (
    <>
      <Button className={styles.uploadButton} onClick={handleUploadFileClick}>
        Upload Data
      </Button>

      {settings?.mock === "true" && (
        <Button className={styles.uploadButton} onClick={handleDownload}>
          Download Mock CSV
        </Button>
      )}

      <input
        ref={inputFileRef}
        hidden
        type="file"
        onChange={handleUploadFile}
        accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
      />
    </>
  );
}
