import styles from "../../styles/Components.module.scss";

export function Loader() {
  return (
    <div className={styles.loader}>
      <div className={styles.backdrop} />
      <div className={styles.content}>
        <div>
          <div>Processing... </div>
          <div>It might take several seconds</div>
          
        </div>
      </div>
    </div>
  );
}
