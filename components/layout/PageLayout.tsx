import * as React from "react";
import { styled, useTheme, Theme, CSSObject } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import { ReactJSXElementChildrenAttribute } from "@emotion/react/types/jsx-namespace";
import InsertChartIcon from "@mui/icons-material/InsertChart";
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";
import { useRouter } from "next/router";
import CalculateIcon from "@mui/icons-material/Calculate";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import DescriptionIcon from "@mui/icons-material/Description";
import SettingsSuggestIcon from "@mui/icons-material/SettingsSuggest";
import BackupIcon from "@mui/icons-material/Backup";
import WidgetsIcon from "@mui/icons-material/Widgets";
import MenuDrawer from "./MenuDrawer";
import Image from "next/image";
import logoautograph from "../../images/logo-autograph.png";
import Footer from "./Footer";
import StorageIcon from "@mui/icons-material/Storage";

const drawerWidth = 240;

const openedMixin = (theme: Theme): CSSObject => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme: Theme): CSSObject => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

type Props = {
  Body: any;
};
export default function MiniDrawer({ Body }: Props) {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const router = useRouter();
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const reDirect = (path: string) => {
    router.push(path);
    // window.location.replace(path);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        {/* <DrawerHeader /> */}
        <MenuDrawer
          FilterUI={() => (
            <List>
              <ListItem
                key={1}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/")}
              >
                <div style={{ display: "flex", padding: "2rem" }}>
                <div className="ag-header">
          <div className="ag-logo">
                <span className="new-logo">
              <Image src={logoautograph} alt="" />
            </span>
            <b className="nav-text-head">Auto-Graph</b>
                </div>
                </div>
                </div>
              </ListItem>
              <Divider style={{ marginTop: 15, marginBottom: 25 }} />
              <ListItem
                key={1}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/")}
              >
                <ListItemButton
                className="active"
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                      
                    }}
                  >
                    <InsertChartIcon style={{ color: "#737791" }} />
                    <p className="menu-text">AI Dashboard</p>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Dashboard"}
                    // secondary="View Dashboaed"
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </ListItem>
              <ListItem
                key={1}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/manual")}
              >
                <ListItemButton
                  className="active"
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <CalculateIcon style={{ color: "#737791" }} />
                    <p className="menu-text">Manual Analysis</p>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Dashboard"}
                    // secondary="View Dashboaed"
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </ListItem>
              <Divider
                style={{ background: "white", marginTop: 15, marginBottom: 15 }}
              />

              <ListItem
                key={1}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/report")}
              >
                <ListItemButton
                  className="active"
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <DescriptionIcon style={{ color: "#737791" }} />
                    <p className="menu-text">Report </p>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Dashboard"}
                    // secondary="View Dashboaed"
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </ListItem>
              <ListItem
                key={1}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/doc")}
              >
                <ListItemButton
                  className="active"
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <MenuBookIcon style={{ color: "#737791" }} />
                    <p className="menu-text">Document Generator</p>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Dashboard"}
                    // secondary="View Dashboaed"
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </ListItem>

              <Divider
                style={{ background: "white", marginTop: 15, marginBottom: 15 }}
              />
              <ListItem
                key={2}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/data")}
              >
                <ListItemButton
                  className="active"
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <BackupIcon style={{ color: "#737791" }} />
                    <p className="menu-text">Upload Data</p>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Data"}
                    // secondary="View Dashboaed"
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </ListItem>
              <ListItem
                key={2}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/sql")}
              >
                <ListItemButton
                  className="active"
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <StorageIcon style={{ color: "#737791" }} />
                    <p className="menu-text">SQL </p>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Data"}
                    // secondary="View Dashboaed"
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </ListItem>
              <ListItem
                key={3}
                disablePadding
                sx={{ display: "block" }}
                onClick={() => reDirect("/settings")}
              >
                <ListItemButton
                  className="active"
                  sx={{
                    minHeight: 48,
                    justifyContent: open ? "initial" : "center",
                    px: 2.5,
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <SettingsSuggestIcon style={{ color: "#737791" }} />
                    <p className="menu-text">Settings</p>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Settings"}
                    // secondary="View Dashboaed"
                    sx={{ opacity: open ? 1 : 0 }}
                  />
                </ListItemButton>
              </ListItem>
            </List>
          )}
        />
        <Body />
        {/* <Footer /> */}
      </Box>
    </Box>
  );
}
