import React from "react";
import styles from "../../styles/Components.module.scss";
import { Button } from "./Button";
import { TextInput } from "./TextInput";
import SelectInput from "./SelectInput";
import { GPT_MODEL, MOCK } from "../../openai/constants";
import swal from "sweetalert";

export function SettingsModal(
  props: React.PropsWithChildren<{
    value: {
      url: string;
      auth: string;
      apikey: string;
      sampleRows: number;

      model: string;
      mock: string;
      context: string;
      noOfGraph: number;

      OPENAI_API_VERSION: string;
      OPENAI_API_BASE: string;
      deployment_name: string;
    };
    onChange?: (value: {
      url: string;
      auth: string;
      apikey: string;
      sampleRows: number;

      model: string;
      mock: string;
      context: string;
      noOfGraph: number;

      OPENAI_API_VERSION: string;
      OPENAI_API_BASE: string;
      deployment_name: string;
    }) => void;
    resetToDefault: any;
    onCancel?: () => void;
  }>
) {
  const [settings, setSettings] = React.useState(props.value);

  const handleUrlChange = React.useCallback((url: string) => {
    setSettings((settings) => ({ ...settings, url }));
  }, []);
  const handleAuthChange = React.useCallback((auth: string) => {
    setSettings((settings) => ({ ...settings, auth }));
  }, []);

  const handleApiKeyChange = React.useCallback((apikey: string) => {
    setSettings((settings) => ({ ...settings, apikey }));
  }, []);

  const handleRowsSampleChange = React.useCallback((sampleRows: number) => {
    setSettings((settings) => ({ ...settings, sampleRows }));
  }, []);

  const handleApiVersionChange = React.useCallback(
    (OPENAI_API_VERSION: string) => {
      setSettings((settings) => ({ ...settings, OPENAI_API_VERSION }));
    },
    []
  );

  const handleOPENAI_API_BASEChange = React.useCallback(
    (OPENAI_API_BASE: string) => {
      setSettings((settings) => ({ ...settings, OPENAI_API_BASE }));
    },
    []
  );
  const handledeployment_nameChange = React.useCallback(
    (deployment_name: string) => {
      setSettings((settings) => ({ ...settings, deployment_name }));
    },
    []
  );

  const handleNoOfGraphChange = React.useCallback((noOfGraph: number) => {
    setSettings((settings) => ({ ...settings, noOfGraph }));
  }, []);

  const handleModelChange = React.useCallback((model: string) => {
    setSettings((settings) => ({ ...settings, model }));
  }, []);

  const handleMockChange = React.useCallback((mock: string) => {
    setSettings((settings) => ({ ...settings, mock }));
  }, []);

  const handleContextKeyChange = React.useCallback((context: string) => {
    setSettings((settings) => ({ ...settings, context }));
  }, []);

  const handleSave = React.useCallback(() => {
    props.onChange?.(settings);
    swal("Success!", "Configration updated!", "success");
  }, [props.onChange, settings]);

  return (
    <div>
      <div>
        <div className={styles.settingsContent}>
          <TextInput
            value={settings.url}
            onChange={handleUrlChange}
            label={<>URL </>}
            type="text"
          />
          <TextInput
            value={settings.apikey}
            onChange={handleApiKeyChange}
            label={<>API Key </>}
            type="text"
          />
          <TextInput
            value={settings.auth}
            onChange={handleAuthChange}
            label={<>Auth token </>}
            type="text"
          />
          <TextInput
            value={settings.OPENAI_API_VERSION}
            onChange={handleApiVersionChange}
            label={<>Api Version </>}
            type="text"
          />
          <TextInput
            value={settings.OPENAI_API_BASE}
            onChange={handleOPENAI_API_BASEChange}
            label={<>Api Base URI </>}
            type="text"
          />
          <TextInput
            value={settings.deployment_name}
            onChange={handledeployment_nameChange}
            label={<>Open AI deployment Name </>}
            type="text"
          />
          {/* <TextInput
            value={settings.context}
            onChange={handleContextKeyChange}
            label={<>Context </>}
            type="text"
          /> */}
          <TextInput
            value={settings.sampleRows}
            onChange={handleRowsSampleChange}
            label="Rows to sample"
            type="number"
          />
          <TextInput
            value={settings.noOfGraph}
            onChange={handleNoOfGraphChange}
            label="Number of Graphs"
            type="number"
          />
          {/* <SelectInput
            onChange={handleModelChange}
            options={Object.values(GPT_MODEL)}
            title="Model"
            value={settings.model}
          /> */}
          <br />
          <SelectInput
            onChange={handleMockChange}
            options={Object.values(MOCK)}
            title="Mock Use"
            value={settings.mock}
          />
        </div>
        <div className={styles.settingsFooter}>
          <br />
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button onClick={handleSave}>Save</Button>
            <Button
              onClick={() => {
                props.resetToDefault();
              }}
            >
              Reset default
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
