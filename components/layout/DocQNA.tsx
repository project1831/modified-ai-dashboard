import React, { useEffect } from "react";
import styles from "../../styles/Components.module.scss";
import { Button } from "./Button";
import { Dashboard_bakeend_base_url } from "../../config";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Image from "next/image";
import loader from "../../Asset/databases.gif";
import DOCChat from "../viz/DOCChat";
import Typography from "@mui/material/Typography";
import DeleteIcon from "@mui/icons-material/Delete";
import RouterIcon from "@mui/icons-material/Router";

import ImageToText from "../viz/ImageToText";

export default function SettingsModal() {
  const [loading, setLoading] = React.useState(false);

  const [selectedFiles, setSelectedFiles] = React.useState([]);
  const [filePreviews, setFilePreviews] = React.useState([]);
  const [summary, setSummary] = React.useState("");
  const [uploaded, setUploaded] = React.useState(false);

  const [textFrmImage, setTextFrmImage] = React.useState([]);

  const fileChangedHandler = (event: any) => {
    const filesArray: any = Array.from(event.target.files); // Convert FileList to Array
    setSelectedFiles(filesArray);

    // Generate previews
    const fileURLs: any = filesArray.map((file: any) =>
      URL.createObjectURL(file)
    );
    setFilePreviews(fileURLs);
  };

  const uploadImageHandler = async () => {
    setLoading(true);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions: any = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify({
        text_data: JSON.stringify(textFrmImage),
      }),
      redirect: "follow",
    };

    return await fetch(
      `${Dashboard_bakeend_base_url}/upload-text`,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        localStorage.removeItem("pdf-chat");
        setUploaded(true);
        setLoading(false);
      })
      .catch((error) => {
        console.log("error", error);
        setLoading(false);
      });
  };

  const uploadHandler = () => {
    setLoading(true);

    const formData = new FormData();

    for (let i = 0; i < selectedFiles.length; i++) {
      // Here, I'm using the selectedFiles directly, not hard-coded paths.
      formData.append("pdf_docs", selectedFiles[i]);
    }

    const requestOptions: any = {
      method: "POST",
      body: formData,
      redirect: "follow",
    };

    fetch(`${Dashboard_bakeend_base_url}/upload-file`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.text();
      })
      .then((result) => {
        console.log(result);
        localStorage.removeItem("pdf-chat");
        setUploaded(true);
        setLoading(false);
      })
      .catch((error) => {
        console.log("error", error);
        setLoading(false);
      });
  };

  const deleteFile = (indexToDelete: any) => {
    setSelectedFiles((prevFiles) => {
      // Convert to Array before applying the filter method
      return prevFiles.filter((_, index) => index !== indexToDelete);
    });

    setFilePreviews((prevPreviews) => {
      const updatedPreviews = prevPreviews.filter(
        (_, index) => index !== indexToDelete
      );
      URL.revokeObjectURL(prevPreviews[indexToDelete]);
      return updatedPreviews;
    });
  };

  console.log("textFrmImage=================>", textFrmImage);
  return (
    <>
      {loading ? (
        <div className="center">
          <div className="center-button-wrapper">
            <center>
              <Image
                src={loader}
                alt="ii"
                style={{ height: 300, width: "100%" }}
              />
              <br />
              Please wait...
            </center>
          </div>
        </div>
      ) : (
        <div>
          <Card style={{ padding: 20, margin: 5, marginTop: 20 }}>
            <div style={{ display: "flex" }}>
              <div
                className={styles.settingsContent}
                style={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <div className={styles.textInput}>
                  <div className={styles.label}>
                    Select and upload multiple PDF
                  </div>
                  <br />
                  <input
                    type="file"
                    accept=".pdf"
                    multiple
                    onChange={fileChangedHandler}
                    style={{
                      fontSize: 12,
                      background: "#80808057",
                      padding: 15,
                    }}
                  />
                </div>
              </div>

              {/* <Image
                src={dburi ? database : error}
                alt="ii"
                style={{ height: 300, width: 400 }}
              /> */}
            </div>
            <div
              className={styles.settingsContent}
              style={{
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div className={styles.textInput}>
                <div className={styles.label}>
                  Select and upload multiple Images
                </div>
                <br />
                <ImageToText setTextFrmImage={setTextFrmImage} />
              </div>
            </div>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
              {filePreviews.map((preview, index) => (
                <div key={index} style={{ margin: "20px 0", display: "grid" }}>
                  <DeleteIcon
                    color="error"
                    onClick={() => deleteFile(index)}
                    style={{ float: "right", margin: 20, position: "absolute" }}
                  />
                  <object
                    data={preview}
                    type="application/pdf"
                    width="300"
                    height="300"
                  >
                    <a href={preview}>Download the PDF</a>
                  </object>
                </div>
              ))}
            </div>
            <div className={styles.settingsFooter}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  marginTop: 25,
                }}
              >
                {(textFrmImage.length > 0 || selectedFiles.length > 0) && (
                  <Button
                    onClick={
                      textFrmImage && textFrmImage.length > 0
                        ? uploadImageHandler
                        : uploadHandler
                    }
                  >
                    <RouterIcon style={{ marginRight: 5 }} /> Upload
                  </Button>
                )}
              </div>
            </div>
          </Card>
          <div>
            {uploaded && (
              <Box sx={{ flexGrow: 1, marginTop: 3 }}>
                <Grid container spacing={2}>
                  <Grid xs={6}>
                    <Card style={{ padding: 20, margin: 20 }}>
                      <Typography
                        sx={{ mt: 4, mb: 2 }}
                        variant="h6"
                        component="div"
                      >
                        Summary
                      </Typography>
                      <>
                        <Typography
                          sx={{ mt: 4, mb: 2 }}
                          variant="h6"
                          component="div"
                          style={{ fontSize: 16, fontWeight: 400 }}
                        >
                          {summary}
                        </Typography>
                      </>
                    </Card>
                  </Grid>
                  <Grid xs={6}>
                    <Card style={{ padding: 20, margin: 20 }}>
                      <Typography
                        sx={{ mt: 4, mb: 2 }}
                        variant="h6"
                        component="div"
                      >
                        Chat with the PDFs
                      </Typography>
                      <DOCChat setSummary={setSummary} />
                    </Card>
                  </Grid>
                </Grid>
              </Box>
            )}
          </div>
        </div>
      )}
    </>
  );
}
