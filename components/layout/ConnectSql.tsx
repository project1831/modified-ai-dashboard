import React, { useEffect } from "react";
import styles from "../../styles/Components.module.scss";
import { Button } from "./Button";
import { Dashboard_bakeend_base_url } from "../../config";
import swal from "sweetalert";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import ButtonDiv from "@mui/material/Button";
import { sanitizeArray } from "../../utils/dataCleansing";
import Image from "next/image";
import loader from "../../Asset/databases.gif";
import database from "../../Asset/falcon_persistent_connection_2x.gif";
import error from "../../Asset/how-it-works-cb-replicator-d365.gif";
import PostAddIcon from "@mui/icons-material/PostAdd";
import SQLChat from "../viz/SQLChat";
import { useCoreData, useConfig } from "../../pages/_app";
import { useRouter } from "next/router";

import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Typography from "@mui/material/Typography";
import FolderIcon from "@mui/icons-material/Folder";
import DeleteIcon from "@mui/icons-material/Delete";
import QueryStatsIcon from "@mui/icons-material/QueryStats";
import VisibilityIcon from "@mui/icons-material/Visibility";
import RouterIcon from "@mui/icons-material/Router";
export default function SettingsModal() {
  const [dburi, setDbUri] = React.useState<any>("");
  const [prompt, setPrompt] = React.useState<any>("");
  const [table, setTables] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const router = useRouter();

  let { coreData, setCoreData } = useCoreData();
  let { settings } = useConfig();

  const handleUrlChange = (url: any) => {
    setDbUri(url.target.value);
  };

  const handleSave = async () => {
    setLoading(true);
    localStorage.setItem("db-uri", dburi);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      dbUri: dburi,
    });

    var requestOptions: any = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    await fetch(`${Dashboard_bakeend_base_url}/connect`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        setTables(result);
        localStorage.setItem("db-table", JSON.stringify(result));
        localStorage.removeItem("sql-chat");
        // swal("Success!", "Configration updated!", "success");
        setLoading(false);
      })
      .catch((error) => {
        console.log("error", error);
        swal("Success!", error, "error");

        setLoading(false);
      });
    setLoading(false);
  };

  useEffect(() => {
    const uri = localStorage.getItem("db-uri");
    const diTable = localStorage.getItem("db-table");
    if (uri) {
      setDbUri(uri);
    }
    if (diTable) {
      setTables(JSON.parse(diTable));
    }
  }, []);

  const fetchTableData = async (tableName: string, type: string) => {
    setLoading(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      dbUri: dburi,
      table: tableName,
    });

    var requestOptions: any = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    await fetch(`${Dashboard_bakeend_base_url}/get-table-data`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        const sanatizeData = sanitizeArray(result);
        setCoreData(sanatizeData?.resultArray);

        localStorage.removeItem("data-summary");
        localStorage.removeItem("gpt-data");
        localStorage.removeItem("report-data");

        router.push(type === "data" ? "/data" : "/");
        // setLoading(false);
      })
      .catch((error) => {
        console.log("error", error);
        setLoading(false);
      });
  };

  return (
    <>
      {loading ? (
        <div className="center">
          <div className="center-button-wrapper">
            <center>
              <Image
                src={loader}
                alt="ii"
                style={{ height: 300, width: "100%" }}
              />
              <br />
              Please wait...
            </center>
          </div>
        </div>
      ) : (
        <div>
          <Card style={{ padding: 20, margin: 5, marginTop: 20 }}>
            <div style={{ display: "flex" }}>
              <div
                className={styles.settingsContent}
                style={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <div className={styles.textInput}>
                  <div className={styles.label}>SQL Connection URI</div>
                  <br />
                  <input
                    type="text"
                    onChange={handleUrlChange}
                    value={dburi}
                    style={{
                      fontSize: 12,
                      background: "#80808057",
                      padding: 15,
                    }}
                  />
                </div>
                <div className={styles.settingsFooter}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      marginTop: 17,
                    }}
                  >
                    <Button onClick={handleSave}>
                      <RouterIcon style={{ marginRight: 5 }} /> Connect
                    </Button>
                  </div>
                </div>
              </div>
              <Image
                src={dburi ? database : error}
                alt="ii"
                style={{ height: 300, width: 400 }}
              />
            </div>
          </Card>
          <div>
            {dburi && table && table?.length > 0 && (
              <Box sx={{ flexGrow: 1, marginTop: 3 }}>
                <Grid container spacing={2}>
                  <Grid xs={6}>
                    <Card style={{ padding: 20, margin: 20 }}>
                      <Typography
                        sx={{ mt: 4, mb: 2 }}
                        variant="h6"
                        component="div"
                      >
                        Tables avialable on the databse
                      </Typography>
                      <>
                        <List style={{ width: "100%" }}>
                          {table &&
                            table.map((val, i) => (
                              <>
                                <ListItem
                                  key={i}
                                  secondaryAction={
                                    <>
                                      <IconButton
                                        edge="end"
                                        aria-label="delete"
                                        style={{ marginRight: 5 }}
                                      >
                                        <VisibilityIcon
                                          color="primary"
                                          onClick={() =>
                                            fetchTableData(val, "data")
                                          }
                                        />
                                      </IconButton>
                                      <IconButton
                                        edge="end"
                                        aria-label="delete"
                                      >
                                        <QueryStatsIcon
                                          color="error"
                                          onClick={() =>
                                            fetchTableData(val, "analize")
                                          }
                                        />
                                      </IconButton>
                                    </>
                                  }
                                >
                                  <ListItemAvatar>
                                    <Avatar>
                                      <PostAddIcon />
                                    </Avatar>
                                  </ListItemAvatar>
                                  <ListItemText
                                    primary={val}
                                    // secondary={secondary ? "Secondary text" : null}
                                  />
                                </ListItem>
                                <hr />
                              </>
                            ))}
                        </List>
                      </>
                    </Card>
                  </Grid>
                  <Grid xs={6}>
                    <Card style={{ padding: 20, margin: 20 }}>
                      <Typography
                        sx={{ mt: 4, mb: 2 }}
                        variant="h6"
                        component="div"
                      >
                        Chat with the database
                      </Typography>
                      <SQLChat dburi={dburi} />
                    </Card>
                  </Grid>
                </Grid>
              </Box>
            )}
          </div>
        </div>
      )}
    </>
  );
}
