import React, { useState, useRef } from "react";
import { Box, Typography, List, ListItem, ListItemText } from "@mui/material";
import MockSuggestion from "../../openai/Mock/suggestion_MOCL.json";
import MockSuggestionResponse from "../../openai/Mock/suggestion_MOCK_full.json";
import Button from "@mui/material/Button";
import ManageSearchIcon from "@mui/icons-material/ManageSearch";
import Modal from "@mui/material/Modal";
import styles from "../../styles/Components.module.scss";
import TextField from "@mui/material/TextField";
import { useConfig } from "../../pages/_app";
import ChartContent from "./ChatContent";
import swal from "sweetalert";
import ChatIcon from "@mui/icons-material/Chat";
import { Dashboard_bakeend_base_url } from "../../config";
import Image from "next/image";
import loader from "../../Asset/chat.gif";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  height: 700,
  overflow: "auto",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const BusinessImprovementList: React.FC<any> = ({ dburi }: any) => {
  const [data, setData] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [chatVal, setChatVal] = React.useState<any>(null);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [question, setQuestion] = useState("");
  let { settings } = useConfig();

  const fetchData = async () => {
    const val = {
      user: "client",
      text: question,
    };
    const chartLocal = localStorage.getItem("sql-chat");
    if (chartLocal) {
      const chat = JSON.parse(chartLocal);
      chat.push(val);
      localStorage.setItem("sql-chat", JSON.stringify(chat));
    } else {
      localStorage.setItem("sql-chat", JSON.stringify([val]));
    }

    setLoading(true);

    if (settings) {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        config: {
          OPENAI_API_VERSION: settings.OPENAI_API_VERSION,
          OPENAI_API_BASE: settings.OPENAI_API_BASE,
          OPENAI_API_KEY: settings.apikey,
          deployment_name: settings.deployment_name,
        },
        dbUri: dburi,
        question: question,
      });

      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      await fetch(`${Dashboard_bakeend_base_url}/ask-question`, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          console.log(result);
          const chartLocal = localStorage.getItem("sql-chat");
          const chat = chartLocal && JSON.parse(chartLocal);
          let resultSave = result.split("Question:")[0].trim();
          const val = {
            user: "AI",
            text: resultSave,
          };
          chat.push(val);
          localStorage.setItem("sql-chat", JSON.stringify(chat));

          setQuestion("");

          setLoading(false);
        })
        .catch((error) => {
          console.log("error", error);
          setLoading(false);
        });
    }
  };

  React.useEffect(() => {
    const chartLocal = localStorage.getItem("sql-chat");
    if (chartLocal) {
      const chat = JSON.parse(chartLocal);
      setChatVal(chat);
    }
  }, [loading]);

  return (
    <>
      {chatVal && <ChartContent chatVal={chatVal} loading={loading} />}
      {loading ? (
        <center>
          <Image src={loader} alt="ii" style={{ height: 150, width: "80%" }} />
        </center>
      ) : (
        <>
          <div
            style={{
              bottom: 20,
              //   position: "absolute",
              width: "100%",
            }}
          >
            <TextField
              id="outlined-basic"
              label="Question"
              variant="outlined"
              fullWidth
              value={question}
              onChange={(e: any) => setQuestion(e.target.value)}
            />
            <Button
              variant="contained"
              onClick={() => fetchData()}
              style={{ marginTop: 10, background: "#ad1b02" }}
              fullWidth
            >
              Ask
            </Button>
          </div>
        </>
      )}
    </>
  );
};

export default BusinessImprovementList;
