import React, { useRef } from "react";
import html2canvas from "html2canvas";
import { IDashboard, IDataset, IDatasetRecord } from "../../types";
import styles from "../../styles/Components.module.scss";
import { DropdownFilter } from "./DropdownFilter";
import { PerformanceIndicator } from "./PerformanceIndicator";
import { LineChart } from "./LineChart";
import { BarChart } from "./BarChart";
import { PieChart } from "./PieChart";
import { className } from "../../utils/className";
import { TreemapChart } from "./TreemapChart";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import DownloadForOfflineIcon from "@mui/icons-material/DownloadForOffline";
import CloseIcon from "@mui/icons-material/Close";
import GetAppIcon from "@mui/icons-material/GetApp";
import TabData from "./Tab";
import GenerateDoc from "./GenerateDoc";
import GenerateReport from "./GenerateReport";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";

import FilterDrawer from "../layout/FilterDrawer";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      style={{ padding: 0 }}
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <div style={{ padding: 0 }}>
          <Typography>{children}</Typography>
        </div>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export function Dashboard(
  props: React.PropsWithChildren<{
    dashboard: IDashboard;
    data: IDataset;
  }>
) {
  const [filters, setFilters] = React.useState<
    Pick<IDatasetRecord, keyof IDatasetRecord>
  >({});

  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const divRef = useRef(null);
  const handleFilterChange = React.useCallback((filter: string) => {
    return (value: string) => {
      setFilters((filters) => ({ ...filters, [filter]: value }));
    };
  }, []);

  const filteredData = React.useMemo(() => {
    if (Object.keys(filters).length) {
      return Object.keys(filters).reduce((result, key) => {
        if (filters[key])
          return result.filter((row) => row[key] == filters[key]);
        return result;
      }, props.data);
    }
    return props.data;
  }, [filters, props.data]);

  const saveAsPNG = (index: any) => {
    // const divToCapture = divRef.current;
    const divToCapture = document.getElementById(index);

    if (divToCapture) {
      html2canvas(divToCapture).then((canvas) => {
        const link = document.createElement("a");
        link.href = canvas.toDataURL("image/png");
        link.download = "captured_div.png";
        link.click();
      });
    }
  };
  const deleteGraph = (index: any) => {
    // const divToCapture = divRef.current;
    const divToCapture = document.getElementById(index);

    if (divToCapture) {
      divToCapture.style.display = "none";
    }
  };

  localStorage.setItem("data-summary", props.dashboard?.summary?.details);

  return (
    <>
      <div className={styles.dashboardContainer}>
        <div className={styles.kpiRow} style={{ marginBottom: "3rem" }}>
          {props.dashboard.kpis.map((filter, index) => (
            <PerformanceIndicator
              key={`${filter.title}-${index}`}
              config={filter}
              data={filteredData}
            />
          ))}
        </div>

        {/* <div className={styles.dashboardContainer} style={{ padding: 0 }}>
          <div
            className={className(styles.chartCard)}
            ref={divRef}
            style={{ height: "150px", padding: 20 }}
          >
            <div className={styles.chartCardTitle}>Summary</div>
            <p style={{ color: "#0808087a" }}>
              {props.dashboard?.summary?.details}
            </p>
          </div>
        </div> */}

        {props.dashboard.charts.map((chart, index) => (
          <div
            key={`${chart.title}-${index}`}
            className={className(styles.chartCard, styles[chart.chartType])}
            ref={divRef}
            id={`${chart.title}-${index}`}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <div
                className={styles.chartCardTitle}
                style={{
                  fontSize: 20,
                  fontWeight: 500,
                }}
              >
                {chart.title}
              </div>

              <div>
                <GetAppIcon
                  onClick={() => saveAsPNG(`${chart.title}-${index}`)}
                  style={{ cursor: "pointer", color: "#737791" , backgroundColor: '#EFEFEF', borderRadius: '20px' , fontSize: 40 , padding: '8px'}}
                />
                <DeleteOutlineIcon
                  onClick={() => deleteGraph(`${chart.title}-${index}`)}
                  style={{
                    cursor: "pointer",
                    color: "#ad1b02",
                    marginLeft: 5, 
                    backgroundColor: '#EFEFEF', 
                    borderRadius: '20px',
                    fontSize: 40,
                    padding: '8px'
                  }}
                />
              </div>
            </div>

            {chart.chartType === "lineChart" && (
              <TabData
                config={chart}
                data={filteredData}
                type={chart.chartType}
              />
            )}
            {chart.chartType === "barChart" && (
              <TabData
                config={chart}
                data={filteredData}
                type={chart.chartType}
              />
            )}
            {chart.chartType === "pieChart" && (
              <TabData
                config={chart}
                data={filteredData}
                type={chart.chartType}
              />
            )}
            {/* {chart.chartType === "treemapChart" && (
              <TreemapChart config={chart} data={filteredData} />
            )} */}
            {chart.chartType === "treemapChart" && (
              <TabData config={chart} data={filteredData} type={"lineChart"} />
              // <TreemapChart config={chart} data={filteredData} />
            )}
          </div>
        ))}
      </div>

      <FilterDrawer
        FilterUI={() => (
          <section
            className="flex-column align-items-stretch h-100 w-100 in-right-section _pb-3"
            style={{ padding: "2rem" }}
          >
            <div className="px-3 in-lp-filter">
              {props.dashboard.filters.map((filter, index) => (
                <DropdownFilter
                  key={`${filter.column}-${index}`}
                  config={filter}
                  data={props.data}
                  onChange={handleFilterChange(filter.column)}
                  value={filters[filter.column]}
                />
              ))}
            </div>
          </section>
        )}
      />
    </>
  );
}
