import * as React from "react";
import Image from "next/image";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";

import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import GraphLoader from "../../layout/GraphLoader";
import { Dashboard_bakeend_base_url } from "../../../config";

const Prediction = ({ filteredData, xAxisData, yAxisData }: any) => {
  const [loading, setLoading] = React.useState(false);
  const [imageData, setImageData] = React.useState(null);
  const [type, setType] = React.useState("linierRegration");

  const handleChange = (event: SelectChangeEvent) => {
    setType(event.target.value as string);
  };

  React.useEffect(() => {
    setLoading(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      data: filteredData,
      x: xAxisData,
      y: yAxisData,
      type: type,
    });

    var requestOptions: any = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(`${Dashboard_bakeend_base_url}/api/prediction`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        // console.log(result);
        setImageData(result);
        setLoading(false);
      })
      .catch((error) => console.log("error", error));
  }, [filteredData, type]);

  console.log("predict=====>", filteredData);
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          {!loading && imageData ? (
            <Image
              src={imageData}
              alt="ii"
              width="500"
              height="500"
              style={{ height: "auto", width: "100%" }}
            />
          ) : (
            <>
              <GraphLoader />
              <p style={{ margin: 30 }}>
                *Graph will generate only if both x and y axis values are
                numaric
              </p>
            </>
          )}
        </Grid>
        <Grid item xs={4}>
          <Box style={{ margin: 30 }}>
            <h4>Predict future values by diffrent methods</h4>
            <br />
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Method</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={type}
                label="Type"
                onChange={handleChange}
              >
                <MenuItem value={"linierRegration"}>Linier Regration</MenuItem>
                <MenuItem value={"polynomialRegression"}>
                  Polynomial Regression
                </MenuItem>
                <MenuItem value={"movingAvarage"}>Moving Avarage</MenuItem>
                <MenuItem value={"simpleExtrapolation"}>
                  Simple Extrapolation
                </MenuItem>
                <MenuItem value={"timeSeriesAnalysis"}>
                  Time Series Analysis
                </MenuItem>
              </Select>
            </FormControl>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

// {imageData && (
//     <CardMedia
//       sx={{ height: 200 }}
//       image={imageData}
//       title="green iguana"
//     />
//   )}

export default Prediction;
