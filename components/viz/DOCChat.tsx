import React, { useState, useRef } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useConfig } from "../../pages/_app";
import ChartContent from "./ChatContent";
import swal from "sweetalert";
import ChatIcon from "@mui/icons-material/Chat";
import { Dashboard_bakeend_base_url } from "../../config";
import Image from "next/image";
import loader from "../../Asset/chat.gif";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  height: 700,
  overflow: "auto",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const BusinessImprovementList: React.FC<any> = ({ setSummary }: any) => {
  const [data, setData] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [chatVal, setChatVal] = React.useState<any>(null);
  const [question, setQuestion] = useState("");
  let { settings } = useConfig();

  const fetchData = async () => {
    setLoading(true);
    const val = {
      user: "client",
      text: question,
    };
    const chartLocal = localStorage.getItem("pdf-chat");
    if (chartLocal) {
      const chat = JSON.parse(chartLocal);
      chat.push(val);
      localStorage.setItem("pdf-chat", JSON.stringify(chat));
    } else {
      localStorage.setItem("pdf-chat", JSON.stringify([val]));
    }

    if (settings) {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        config: {
          OPENAI_API_VERSION: settings.OPENAI_API_VERSION,
          OPENAI_API_BASE: settings.OPENAI_API_BASE,
          OPENAI_API_KEY: settings.apikey,
          deployment_name: settings.deployment_name,
        },
        user_question: question,
      });

      let result: any = "";
      result = await requestData(raw);
      console.log("========result=======>", result);
      const chartLocal = localStorage.getItem("pdf-chat");
      const chat = chartLocal && JSON.parse(chartLocal);
      //   let resultSave = result?.split("Question:")[0].trim();
      const val = {
        user: "AI",
        text: result,
      };

      chat.push(val);
      localStorage.setItem("pdf-chat", JSON.stringify(chat));

      setQuestion("");

      setLoading(false);
    }
  };
  const requestData = async (raw: any) => {
    if (settings) {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      return await fetch(
        `${Dashboard_bakeend_base_url}/process`,
        requestOptions
      )
        .then((response) => response.json())
        .then((result) => {
          //   let resultSave = result.split("Question:")[0].trim();
          return result;
        })
        .catch((error) => {
          console.log("error", error);
          return error;
        });
    }
  };

  React.useEffect(() => {
    const chartLocal = localStorage.getItem("pdf-chat");
    if (chartLocal) {
      const chat = JSON.parse(chartLocal);
      setChatVal(chat);
    }
  }, [loading]);

  React.useEffect(() => {
    getSummary();
  }, []);

  const getSummary = async () => {
    var raw = JSON.stringify({
      config: {
        OPENAI_API_VERSION: settings.OPENAI_API_VERSION,
        OPENAI_API_BASE: settings.OPENAI_API_BASE,
        OPENAI_API_KEY: settings.apikey,
        deployment_name: settings.deployment_name,
      },
      user_question:
        "summarize all the documents separately in diffrent paragraph with a space between each",
    });
    const summary = await requestData(raw);
    setSummary(summary);
  };

  //
  return (
    <>
      {chatVal && <ChartContent chatVal={chatVal} loading={loading} />}
      {loading ? (
        <center>
          <Image src={loader} alt="ii" style={{ height: 150, width: "80%" }} />
        </center>
      ) : (
        <>
          <div
            style={{
              bottom: 20,
              //   position: "absolute",
              width: "100%",
            }}
          >
            <TextField
              id="outlined-basic"
              label="Question"
              variant="outlined"
              fullWidth
              value={question}
              onChange={(e: any) => setQuestion(e.target.value)}
            />
            <Button
              variant="contained"
              onClick={() => fetchData()}
              style={{ marginTop: 10, background: "#ad1b02" }}
              fullWidth
            >
              Ask
            </Button>
          </div>
        </>
      )}
    </>
  );
};

export default BusinessImprovementList;
