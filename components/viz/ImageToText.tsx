import { useCallback, useEffect, useState } from "react";
import { createWorker } from "tesseract.js";

function App({ setTextFrmImage }: any) {
  const [selectedImages, setSelectedImages] = useState<any>([]); // Change to an array
  const [textResults, setTextResults] = useState([]); // Change to an array
  const [loader, setLoader] = useState(false);
  const worker = createWorker();

  const convertImageToText = useCallback(async () => {
    setLoader(true);
    const results: any = [];
    for (let image of selectedImages) {
      await worker.load();
      await worker.loadLanguage("eng");
      await worker.initialize("eng");
      const { data } = await worker.recognize(image);
      results.push(data.text);
    }
    setTextResults(results);
    setLoader(false);
  }, [worker, selectedImages]);

  useEffect(() => {
    if (selectedImages.length > 0) {
      convertImageToText();
    }
  }, [selectedImages, convertImageToText]);

  const handleChangeImage = (e: any) => {
    if (e.target.files.length > 0) {
      setSelectedImages([...e.target.files]);
    } else {
      setSelectedImages([]);
      setTextResults([]);
    }
  };

  useEffect(() => {
    setTextFrmImage(textResults);
  }, [textResults, textResults.length]);

  return (
    <div>
      <div className="input-wrapper">
        <input
          type="file"
          id="upload"
          accept="image/*"
          onChange={handleChangeImage}
          style={{
            fontSize: 12,
            background: "#80808057",
            padding: 15,
          }}
          multiple // Allow multiple file selection
        />
      </div>

      <div style={{ display: "flex", justifyContent: "space-around" }}>
        {selectedImages.map((image: any, index: any) => (
          <div key={index}>
            <div className="box-image">
              <img
                src={URL.createObjectURL(image)}
                alt="thumb"
                style={{ height: "auto", width: 350, margin: 10 }}
              />
            </div>
            {loader && "Please wait..."}
            {textResults[index] && (
              <div className="box-p">
                <p>{textResults[index]}</p>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
