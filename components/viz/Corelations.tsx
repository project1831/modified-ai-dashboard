import React, { Suspense } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import styles from "../../styles/Components.module.scss";

import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";

import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import GraphLoader from "../layout/GraphLoader";
import loader from "../../Asset/strategy-main.gif";
import Image from "next/image";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const CoRelationGraph = React.lazy(() => import("./sub/CoRelationGraph"));
const ShowTopGraphs = React.lazy(() => import("./sub/ShowTopGraphs"));

interface Props {
  combinations: string[][];
  iniStateData: any;
}

interface DataRecord {
  [key: string]: number | string;
}

function containsNumericValues(dataArray: any, xAxis: any, yAxis: any) {
  for (let item of dataArray) {
    let profitIsNumeric = false;
    let costIsNumeric = false;

    // Try to parse xAxis value
    if (typeof item[xAxis] === "string") {
      const parsedProfit = parseFloat(item[xAxis]);
      if (!isNaN(parsedProfit)) {
        profitIsNumeric = true;
      }
    } else if (typeof item[xAxis] === "number") {
      profitIsNumeric = true;
    }

    // Try to parse yAxis value
    if (typeof item[yAxis] === "string") {
      const parsedCost = parseFloat(item[yAxis]);
      if (!isNaN(parsedCost)) {
        costIsNumeric = true;
      }
    } else if (typeof item[yAxis] === "number") {
      costIsNumeric = true;
    }

    // If either of the values is numeric, return true
    if (profitIsNumeric || costIsNumeric) {
      return true;
    }
  }

  // If none of the items contain numeric values, return false
  return false;
}

const CorrelationCoefficientsTable: React.FC<Props> = ({
  combinations,
  iniStateData,
}) => {
  const twoValuesData = combinations.filter((val) => val.length == 2);

  return (
    <div style={{ marginTop: 20 }}>
      <Box sx={{ flexGrow: 1 }}>
        {/* <Grid container spacing={2} style={{ marginBottom: 40 }}>
          {twoValuesData.splice(0, 10).map((combination, index) => {
            let chartData = iniStateData;

            const groupedData: { [key: string]: number } = {};
            iniStateData.forEach((item: any) => {
              const key = item[combination[0]];
              const value = parseFloat(item[combination[1]]);
              if (!isNaN(value)) {
                groupedData[key] = (groupedData[key] || 0) + value;
              }
            });

            chartData = Object.keys(groupedData).map((key) => ({
              [combination[0]]: key,
              [combination[1]]: groupedData[key],
            }));

            const filteredData = chartData.filter(
              (item: any) => item[combination[0]] && item[combination[1]]
            );
            if (
              containsNumericValues(
                filteredData,
                combination[0],
                combination[1]
              )
            )
              return (
                <Grid item xs={4} key={index}>
                  {iniStateData && combination.length == 2 && (
                    <Suspense fallback={<GraphLoader />}>
                      <ShowTopGraphs
                        iniStateData={filteredData}
                        graph={combination}
                      />
                    </Suspense>
                  )}
                </Grid>
              );
          })}
        </Grid> */}

        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>Vew All Effective Combinations</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={2}>
              {twoValuesData.map((combination, index) => {
                let chartData = iniStateData;

                const groupedData: { [key: string]: number } = {};
                iniStateData.forEach((item: any) => {
                  const key = item[combination[0]];
                  const value = parseFloat(item[combination[1]]);
                  if (!isNaN(value)) {
                    groupedData[key] = (groupedData[key] || 0) + value;
                  }
                });

                chartData = Object.keys(groupedData).map((key) => ({
                  [combination[0]]: key,
                  [combination[1]]: groupedData[key],
                }));

                const filteredData = chartData.filter(
                  (item: any) => item[combination[0]] && item[combination[1]]
                );
                if (
                  containsNumericValues(
                    filteredData,
                    combination[0],
                    combination[1]
                  )
                )
                  return (
                    <Grid item xs={2} key={index}>
                      <Card>
                        <CardContent>
                          <Typography
                            variant="h6"
                            component="div"
                            style={{ fontSize: 12 }}
                          >
                            {combination.join(" / ")}
                          </Typography>
                        </CardContent>
                        <CardActions style={{ margin: 5 }}>
                          {iniStateData && combination.length == 2 && (
                            <Suspense fallback={<div>Loading...</div>}>
                              <CoRelationGraph
                                iniStateData={iniStateData}
                                graph={combination}
                              />
                            </Suspense>
                          )}
                        </CardActions>
                      </Card>
                    </Grid>
                  );
              })}
            </Grid>
          </AccordionDetails>
        </Accordion>
      </Box>
    </div>
  );
};

function findNumericColumnCombinations(data: any): string[][] {
  const numericColumns: string[] = [];

  // Identify numeric columns
  if (data?.length > 0) {
    const sampleObject = data[0];
    for (const key in sampleObject) {
      const value = sampleObject[key];
      // if (!isNaN(Number(value))) {
      numericColumns.push(key);
      // }
    }
  }

  // Find all combinations of numeric columns
  const columnCombinations: string[][] = [];
  const totalColumns = numericColumns.length;

  for (let i = 1; i <= totalColumns; i++) {
    const combinations = getCombinations(numericColumns, i);
    columnCombinations.push(...combinations);
  }

  return columnCombinations;
}

function getCombinations(arr: string[], k: number): string[][] {
  const result: string[][] = [];
  const current: string[] = [];
  function backtrack(start: number) {
    if (current.length === k) {
      result.push([...current]);
      return;
    }
    for (let i = start; i < arr.length; i++) {
      current.push(arr[i]);
      backtrack(i + 1);
      current.pop();
    }
  }
  backtrack(0);
  return result;
}

interface ChildComponentProps {
  stateData: any;
}

const App: React.FC<ChildComponentProps> = ({ stateData }) => {
  const numericColumnCombinations = findNumericColumnCombinations(stateData);

  return (
    <>
      <CorrelationCoefficientsTable
        combinations={numericColumnCombinations}
        iniStateData={stateData}
      />
    </>
  );
};

export default App;
