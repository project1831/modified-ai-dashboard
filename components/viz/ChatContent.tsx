import * as React from "react";

interface HtmlDisplayProps {
  htmlContent: any;
}
const HtmlDisplayComponent: React.FC<HtmlDisplayProps> = ({ htmlContent }) => {
  return <div dangerouslySetInnerHTML={{ __html: htmlContent }} />;
};

export default function ChartContent({ chatVal, loading }: any) {
  const scrollContainerRef: any = React.useRef(null);

  React.useEffect(() => {
    if (scrollContainerRef.current) {
      // Scroll to the bottom
      scrollContainerRef.current.scrollTop =
        scrollContainerRef.current.scrollHeight;
    }
  }, [loading, chatVal]);

  return (
    <div style={{ height: 500, overflowY: "auto" }} ref={scrollContainerRef}>
      {chatVal &&
        chatVal?.map((data: any, index: number) => {
          return (
            <div
              key={index}
              style={
                data?.user === "client"
                  ? {
                      textAlign: "right",
                      width: "80%",
                      float: "right",
                    }
                  : { textAlign: "left", width: "80%", float: "left" }
              }
            >
              <div
                className={
                  data?.user === "client" ? "chat-back-1" : "chat-back-2"
                }
                style={{
                  border: "1px solid #80808066",
                  margin: 10,

                  borderRadius: 8,
                  padding: 10,
                }}
              >
                <HtmlDisplayComponent htmlContent={data?.text} />
              </div>
            </div>
          );
        })}
    </div>
  );
}
