import React, { useState, useEffect, Suspense } from "react";
import {
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Checkbox,
  FormControlLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Card,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
} from "@mui/material";
import { palette } from "../../../utils/palette";
import BarChartData from "../customChart/Bar";

import Radar from "../customChart/Radar";
import PieChart from "../customChart/PieChart";

import Modal from "@mui/material/Modal";
import Grid from "@mui/material/Grid";

import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import CancelIcon from "@mui/icons-material/Cancel";
import BarChartIcon from "@mui/icons-material/BarChart";

import GraphLoader from "../../layout/GraphLoader";

const CustomizeChart = React.lazy(
  () => import("../customChart/CustomizeChart")
);

const DynamicBarChart: React.FC<any> = ({ iniStateData, graph }: any) => {
  const [xAxis, setXAxis] = useState<string>(graph[1]);
  const [yAxis, setYAxis] = useState<string>(graph[0]);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    const takeSample = iniStateData[0];
    const parsed1 = parseFloat(takeSample[graph[0]]);
    const parsed2 = parseFloat(takeSample[graph[1]]);
    if (typeof takeSample[graph[0]] === "string" || isNaN(parsed1)) {
      setXAxis(graph[0]);
      setYAxis(graph[1]);
    } else if (typeof takeSample[graph[1]] === "string" || isNaN(parsed2)) {
      setXAxis(graph[1]);
      setYAxis(graph[0]);
    }
  }, []);

  const headers =
    iniStateData && iniStateData?.length > 0 && Object.keys(iniStateData[0]);

  return (
    <div>
      <Button
        onClick={handleOpen}
        variant="outlined"
        color="warning"
        endIcon={<BarChartIcon />}
      >
        View
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        style={{ overflowY: "auto" }}
      >
        <center>
          <Grid
            container
            spacing={2}
            style={{ marginTop: 30, width: "90%", overflowY: "auto" }}
          >
            <Grid item xs={12}>
              <Button
                onClick={handleClose}
                variant="contained"
                color="error"
                style={{ float: "right" }}
                endIcon={<CancelIcon />}
              >
                Close
              </Button>
              {iniStateData && (
                <Suspense fallback={<GraphLoader />}>
                  <CustomizeChart
                    stateData={iniStateData}
                    xAxisData={xAxis}
                    yAxisData={yAxis}
                  />
                </Suspense>
              )}
            </Grid>
          </Grid>
        </center>
      </Modal>
      <br />
    </div>
  );
};

export default DynamicBarChart;
