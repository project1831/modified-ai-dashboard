import React, { useState, useEffect } from "react";
import {
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Checkbox,
  FormControlLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Card,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
} from "@mui/material";
import { palette } from "../../../utils/palette";
import BarChartData from "../customChart/Bar";

import Radar from "../customChart/Radar";
import PieChart from "../customChart/PieChart";

import Modal from "@mui/material/Modal";
import Grid from "@mui/material/Grid";

import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import CancelIcon from "@mui/icons-material/Cancel";
import BarChartIcon from "@mui/icons-material/BarChart";

const DynamicBarChart: React.FC<any> = ({ iniStateData, graph }: any) => {
  const [xAxis, setXAxis] = useState<string>(graph[1]);
  const [yAxis, setYAxis] = useState<string>(graph[0]);
  const [groupData, setGroupData] = useState<boolean>(true);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const getXAxisValues = (): string[] => {
    return graph;
  };

  const handleXAxisChange = (event: any, value: any) => {
    setXAxis(event.target.value as string);
    setYAxis(value as string);
  };

  const handleYAxisChange = (event: any, value: any) => {
    setYAxis(event.target.value as string);
    setXAxis(value as string);
  };

  const handleGroupDataChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setGroupData(event.target.checked);
  };

  useEffect(() => {
    const takeSample = iniStateData[0];

    const parsed1 = parseFloat(takeSample[graph[0]]);
    const parsed2 = parseFloat(takeSample[graph[1]]);

    if (typeof takeSample[graph[0]] === "string" || isNaN(parsed1)) {
      setXAxis(graph[0]);
      setYAxis(graph[1]);
    } else if (typeof takeSample[graph[1]] === "string" || isNaN(parsed2)) {
      setXAxis(graph[1]);
      setYAxis(graph[0]);
    }
  }, []);

  let chartData = iniStateData;
  if (groupData) {
    const groupedData: { [key: string]: number } = {};
    iniStateData.forEach((item: any) => {
      const key = item[xAxis];
      const value = parseFloat(item[yAxis]);
      if (!isNaN(value)) {
        groupedData[key] = (groupedData[key] || 0) + value;
      }
    });

    chartData = Object.keys(groupedData).map((key) => ({
      [xAxis]: key,
      [yAxis]: groupedData[key],
    }));
  }

  const headers =
    iniStateData && iniStateData?.length > 0 && Object.keys(iniStateData[0]);

  const calculateStatistics = (columnKey: any) => {
    // Extract the 'y' values from the data based on the dynamic columnKey
    const yValues = iniStateData.map((entry: any) => entry[columnKey]);

    const total = yValues.reduce((sum: any, value: any) => sum + value, 0);

    // Calculate the mean
    const mean =
      yValues.reduce((sum: any, value: any) => sum + value, 0) / yValues.length;

    // Calculate the median
    const sortedYValues = [...yValues].sort((a, b) => a - b);
    const middleIndex = Math.floor(sortedYValues.length / 2);
    const median =
      sortedYValues.length % 2 === 0
        ? (sortedYValues[middleIndex - 1] + sortedYValues[middleIndex]) / 2
        : sortedYValues[middleIndex];

    // Calculate the max and min
    const max = Math.max(...yValues);
    const min = Math.min(...yValues);

    return { total, mean, median, max, min };
  };
  const Y_statistics = calculateStatistics(headers[1]);
  return (
    <div>
      <center>
        <Grid
          container
          spacing={2}
          style={{ marginTop: 30, overflowY: "auto" }}
        >
          <Grid item xs={12}>
            <Card
              style={{
                // width: "90%",
                // maxWidth: 360,
                padding: 20,
              }}
            >
              <b> {xAxis}</b> vs <b>{yAxis}</b>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <FormControl sx={{ minWidth: 200, marginRight: 5 }}>
                  <p style={{ marginBottom: 5, fontSize: 14 }}>
                    Select X Axis{" "}
                    <b style={{ color: palette[0] }}>( String / Numeric )</b>
                  </p>

                  <Select
                    value={xAxis}
                    onChange={(e) => handleXAxisChange(e, xAxis)}
                  >
                    {getXAxisValues().map((value) => (
                      <MenuItem key={value} value={value}>
                        {value}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>

                <FormControl sx={{ minWidth: 200 }}>
                  <p style={{ marginBottom: 5, fontSize: 14 }}>
                    Select Y Axis{" "}
                    <b style={{ color: palette[0] }}>( Numeric )</b>
                  </p>

                  <Select
                    value={yAxis}
                    onChange={(e) => handleYAxisChange(e, yAxis)}
                  >
                    {getXAxisValues().map((value: any) => (
                      <MenuItem key={value} value={value}>
                        {value}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
              <BarChartData
                filteredData={chartData}
                xAxis={xAxis}
                yAxis={yAxis}
              />
            </Card>
          </Grid>
        </Grid>
      </center>

      <br />
    </div>
  );
};

export default DynamicBarChart;
