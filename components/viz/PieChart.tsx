import React from "react";
import { IChart, IDataset } from "../../types";
import styles from "../../styles/Components.module.scss";
import {
  PieChart as RPieChart,
  ResponsiveContainer,
  Pie,
  Tooltip,
  Cell,
  Legend,
} from "recharts";
import { parseFunc } from "../../utils/parseFunc";
import { ErrorBoundary } from "../layout/ErrorBoundary";
import { palette } from "../../utils/palette";
import { formatNumber } from "../../utils/numberFormatter";
import Suggestions from "./Suggestion";
import { useChatData, useReportData } from "../../pages/_app";
import DateAnalitics from "./DateAnalitics";

export function PieChart(
  props: React.PropsWithChildren<{
    config: IChart;
    data: IDataset;
    chartSelect: any;
  }>
) {
  const { chatData } = useChatData();
  const { reportData } = useReportData();

  const myGroupingFunction = React.useMemo(() => {
    return parseFunc(props.config.javascriptFunction, (data: IDataset) => data);
  }, [props.config]);

  const data = React.useMemo(() => {
    if (typeof myGroupingFunction === "function")
      return myGroupingFunction(props.data);
    return null;
  }, [myGroupingFunction, props.config, props.data]);

  React.useEffect(() => {
    const val = {
      title: props.config?.title,
      data: data,
    };

    const ifExist = chatData?.find(
      (val: any) => val?.title === props.config?.title
    );

    if (!ifExist) {
      chatData.push(val);
      reportData.push({ ...val, type: props.config?.chartType });
    }
  }, [data]);

  if (!data) return null;

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index,
  }: any) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="white"
        fontSize={12}
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

  props?.chartSelect(data?.length);
  return (
    <ErrorBoundary>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "end",
        }}
      >
        <Suggestions chartData={data} title={props.config?.title} />
        {data && data?.length > 0 && (
          <span style={{ margin: 15 }}>
            {" "}
            <DateAnalitics filteredData={data} />
          </span>
        )}
      </div>
      <ResponsiveContainer width="100%" height="100%">
        <RPieChart width={400} height={400}>
          <Legend verticalAlign="top" height={36} />
          <Pie
            data={data}
            nameKey={"x"}
            dataKey={"y"}
            cx="50%"
            cy="50%"
            outerRadius={90}
            innerRadius={30}
            fill="#8884d8"
            stroke="var(--borderColor)"
            // label={{ fill: "black", fontSize: 12 }}
            labelLine={false}
            label={renderCustomizedLabel}
          >
            {data.map((entry, index) => (
              <Cell
                key={`cell-${index}`}
                fill={palette[index % palette.length]}
              />
            ))}
          </Pie>
          <Tooltip formatter={(value) => formatNumber(value as number)} />
        </RPieChart>
      </ResponsiveContainer>
    </ErrorBoundary>
  );
}
