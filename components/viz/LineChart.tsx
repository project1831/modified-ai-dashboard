import React from "react";
import { IChart, IDataset } from "../../types";
import styles from "../../styles/Components.module.scss";

import {
  CartesianGrid,
  Legend,
  Line,
  LineChart as RLineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
  AreaChart,
  Area,
  Brush,
} from "recharts";
import { parseFunc } from "../../utils/parseFunc";
import { ErrorBoundary } from "../layout/ErrorBoundary";
import { palette } from "../../utils/palette";
import { formatNumber } from "../../utils/numberFormatter";
import Suggestions from "./Suggestion";

import { useChatData, useReportData } from "../../pages/_app";

import DateAnalitics from "./DateAnalitics";

export function LineChart(
  props: React.PropsWithChildren<{
    config: IChart;
    data: IDataset;
    chartSelect: any;
  }>
) {
  const { chatData } = useChatData();
  const { reportData } = useReportData();

  const myGroupingFunction = React.useMemo(() => {
    return parseFunc(props.config.javascriptFunction, (data: IDataset) => data);
  }, [props.config]);

  const data = React.useMemo(() => {
    if (myGroupingFunction && typeof myGroupingFunction === "function") {
      try {
        return myGroupingFunction(props?.data);
      } catch (error) {
        return null;
      }
    } else {
      return null;
    }
  }, [myGroupingFunction, props.config, props.data]);

  React.useEffect(() => {
    const val = {
      title: props.config?.title,
      data: data,
    };
    const ifExist = chatData?.find(
      (val: any) => val?.title === props.config?.title
    );

    if (!ifExist) {
      chatData.push(val);
      reportData.push({ ...val, type: props.config?.chartType });
    }
  }, [data]);

  if (!data) return null;

  props?.chartSelect(data?.length);

  return (
    <ErrorBoundary>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "end",
        }}
      >
        <Suggestions chartData={data} title={props.config?.title} />
        {data && data?.length > 0 && (
          <span style={{ margin: 15 }}>
            {" "}
            <DateAnalitics filteredData={data} />
          </span>
        )}
      </div>

      <ResponsiveContainer width="100%" height="100%">
        <AreaChart width={500} height={500} data={data}>
          <XAxis
            dataKey={"x"}
            stroke="var(--textColor)"
            tick={{ fontSize: 8 }}
          />
          <YAxis
            stroke="var(--textColor)"
            tickFormatter={formatNumber}
            tick={{ fontSize: 8 }}
          />
          <Tooltip />
          <CartesianGrid stroke="var(--borderColor)" strokeDasharray="5 5" />
          <Area
            type="monotone"
            fill="#F39C12"
            dataKey={"y"}
            stroke={palette[1]}
            dot={false}
            label={
              data?.length < 10 && { fill: "black", fontSize: 12, offSet: 5 }
            }
            // connectNulls={true}
          />
          <Brush dataKey="x" height={30} stroke="#8884d8" />
        </AreaChart>
      </ResponsiveContainer>
    </ErrorBoundary>
  );
}
