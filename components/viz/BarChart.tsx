import React, { useRef } from "react";
import html2canvas from "html2canvas";
import { IChart, IDataset } from "../../types";
import {
  CartesianGrid,
  Legend,
  BarChart as RBarChart,
  XAxis,
  YAxis,
  Bar,
  Tooltip,
  ResponsiveContainer,
  Brush,
} from "recharts";
import { parseFunc } from "../../utils/parseFunc";
import { ErrorBoundary } from "../layout/ErrorBoundary";
import { palette } from "../../utils/palette";
import { formatNumber } from "../../utils/numberFormatter";
import Suggestions from "./Suggestion";
import { useChatData, useReportData } from "../../pages/_app";

import DateAnalitics from "./DateAnalitics";

export function BarChart(
  props: React.PropsWithChildren<{
    config: IChart;
    data: IDataset;
    chartSelect: any;
  }>
) {
  const { chatData } = useChatData();
  const { reportData } = useReportData();

  const myGroupingFunction = React.useMemo(() => {
    return parseFunc(
      props?.config?.javascriptFunction,
      (data: IDataset) => data
    );
  }, [props.config]);

  const data = React.useMemo(() => {
    if (myGroupingFunction && typeof myGroupingFunction === "function") {
      try {
        return myGroupingFunction(props?.data);
      } catch (error) {
        return null;
      }
    } else {
      return null;
    }
  }, [myGroupingFunction, props.config, props.data]);

  React.useEffect(() => {
    const val = {
      title: props.config?.title,
      data: data,
    };

    const ifExist = chatData?.find(
      (val: any) => val?.title === props.config?.title
    );

    if (!ifExist) {
      chatData.push(val);
      reportData.push({ ...val, type: props.config?.chartType });
    }
  }, [data]);

  if (!data) return null;

  const CustomLabel = ({ x, y, width, value }: any) => (
    <text
      x={x + width / 2}
      y={y - 10}
      fill="black"
      textAnchor="middle"
      dominantBaseline="middle"
      fontSize={10}
    >
      {parseFloat(value).toFixed(1)}
    </text>
  );

  function getRandomNumber(): number {
    return Math.floor(Math.random() * 6); // Generates a random number between 0 and 5 (inclusive)
  }
  props?.chartSelect(data?.length);

  return (
    <ErrorBoundary>
      <div
        // style={{
        //   display: "flex",
        //   justifyContent: "flex-start",
        //   alignItems: "end",
        // }}
        className="at-chartSrcbtn"
      >
        <Suggestions chartData={data} title={props.config?.title} />
        {data && data?.length > 0 && (
          <span style={{ margin: 15 }}>
            {" "}
            <DateAnalitics filteredData={data} />
          </span>
        )}
      </div>

      <ResponsiveContainer width="100%" height="100%">
        <RBarChart width={500} height={500} data={data}>
          <XAxis
            stroke="var(--textColor)"
            dataKey={"x"}
            tick={{ fontSize: 8 }}
          />
          <YAxis
            stroke="var(--textColor)"
            tickFormatter={formatNumber}
            tick={{ fontSize: 10 }}
          />
          <Tooltip
            contentStyle={{
              background: "rgba(255, 255, 255, 0.8)",
              borderColor: "white",
            }}
            itemStyle={{ color: "black" }}
          />
          {/* <CartesianGrid
            strokeDasharray="3 3"
            stroke="rgba(255, 255, 255, 0.3)"
          /> */}
          <Bar
            dataKey={"y"}
            fill="#ad1b02"
            background={{ fill: "#eee" }}
            // barSize={60}
            // label={{ position: "top", fill: "black", fontSize: 12 }}
            label={data?.length < 10 && <CustomLabel />}
          />
          <Brush dataKey="x" height={30} stroke="#8884d8" />
        </RBarChart>
      </ResponsiveContainer>
    </ErrorBoundary>
  );
}
