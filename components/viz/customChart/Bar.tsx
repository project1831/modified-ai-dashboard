import React, { useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Label,
  ResponsiveContainer,
  PieChart,
  Pie,
  Cell,
  ReferenceLine,
  Brush,
} from "recharts";

import { palette } from "../../../utils/palette";

import DateAnalitics from "../DateAnalitics";

const CustomLabel = ({ x, y, width, value }: any) => (
  <text
    x={x + width / 2}
    y={y - 10}
    fill="black"
    textAnchor="middle"
    dominantBaseline="middle"
    fontSize={12}
  >
    {parseFloat(value).toFixed(1)}
  </text>
);

export default function BarChartData({ filteredData, xAxis, yAxis }: any) {
  // ---------------------------------------------------------------------------------------------------------------------------------

  return (
    <>
      {filteredData && filteredData?.length > 0 && (
        <div style={{ padding: 20, marginTop: 25 }}>
          <DateAnalitics filteredData={filteredData} />
        </div>
      )}
      <div style={{ height: "400px", padding: 20 }}>
        <ResponsiveContainer width="100%" height="100%">
          <BarChart width={600} height={400} data={filteredData}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey={xAxis}>
              <Label
                value={xAxis}
                position="insideBottom"
                offset={-10}
                fontSize={14}
                fill={palette[0]}
              />
            </XAxis>
            <YAxis>
              <Label
                value={yAxis}
                position="insideLeft"
                angle={-90}
                offset={0}
                fontSize={14}
                fill={palette[0]}
              />
            </YAxis>
            <Tooltip />
            {/* <Legend /> */}

            <Bar
              dataKey={yAxis}
              fill={palette[0]}
              label={filteredData?.length < 20 && <CustomLabel />}
              fontSize={12}
            />
            <Brush dataKey={xAxis} height={30} stroke="#8884d8" />
          </BarChart>
        </ResponsiveContainer>
      </div>
    </>
  );
}
