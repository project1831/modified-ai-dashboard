import React from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import styles from "../../../styles/Components.module.scss";

// Assuming all data items have the same keys

const SelectsByKeys: React.FC<any> = ({
  data,
  getFilters,
  filtersInfo,
}: any) => {
  console.log("filtersInfo", filtersInfo);

  const [filters, setFilters] = React.useState<{ [key: string]: string }>({});

  const handleSelectChange = (event: any, key: string) => {
    const selectedValue = event.target.value as string;
    const updatedFilters = { ...filters };

    if (selectedValue === "") {
      delete updatedFilters[key];
    } else {
      updatedFilters[key] = selectedValue;
    }
    getFilters(updatedFilters);
    setFilters(updatedFilters);
  };

  // Create an object to store the grouped values for each key
  const groupedValues: { [key: string]: string[] } = {};

  // Group the values for each key
  data.forEach((item: any) => {
    Object.keys(item).forEach((key) => {
      if (!groupedValues[key]) {
        groupedValues[key] = [];
      }
      if (!groupedValues[key].includes(item[key])) {
        groupedValues[key].push(item[key]);
      }
    });
  });

  return (
    <>
      {Object.keys(groupedValues).map((key, index) => (
        <div style={{ display: "grid" }} key={index}>
          <label style={{ fontSize: 15, color: "white" }}>{key}</label>
          <select
            value={(filtersInfo && filtersInfo[key]) || ""}
            onChange={(event) => handleSelectChange(event, key)}
          >
            <option key={"None"} value="">
              -- Select --
            </option>
            {groupedValues[key].map((value, index) => (
              <option key={index} value={value}>
                {value}
              </option>
            ))}
          </select>
        </div>
      ))}
    </>
  );
};

export default SelectsByKeys;
