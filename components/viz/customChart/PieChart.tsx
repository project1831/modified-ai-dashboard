import React from "react";
import { IChart, IDataset } from "../../../types";
import styles from "../../styles/Components.module.scss";
import {
  PieChart as RPieChart,
  ResponsiveContainer,
  Pie,
  Tooltip,
  Cell,
  Legend,
} from "recharts";
import { parseFunc } from "../../../utils/parseFunc";
import { ErrorBoundary } from "../../layout/ErrorBoundary";
import { palette } from "../../../utils/palette";
import { formatNumber } from "../../../utils/numberFormatter";

export default function PieChart({ filteredData, xAxis, yAxis }: any) {
  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index,
  }: any) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="white"
        fontSize={12}
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}% `}
      </text>
    );
  };

  return (
    <div style={{ height: "500px", marginTop: "20px" }}>
      <ResponsiveContainer>
        <RPieChart>
          <Pie
            data={filteredData}
            nameKey={xAxis}
            dataKey={yAxis}
            cx="50%"
            cy="50%"
            // outerRadius={120}
            // innerRadius={0}
            fill="#8884d8"
            stroke="var(--borderColor)"
            // label={{ fill: "black", fontSize: 12 }}
            labelLine={false}
            label={renderCustomizedLabel}
            legendType="line"
          >
            {filteredData.map((entry: any, index: number) => (
              <Cell
                key={`cell-${index}`}
                fill={palette[index % palette.length]}
              />
            ))}
          </Pie>
          <Tooltip formatter={(value) => formatNumber(value as number)} />
          {/* <Legend  /> */}
        </RPieChart>
      </ResponsiveContainer>
    </div>
  );
}
