import React from "react";
import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  ResponsiveContainer,
} from "recharts";

export function SimpleRadar({ filteredData, xAxis, yAxis }: any) {
  return (
    <div style={{ height: "400px", marginTop: "20px" }}>
      <ResponsiveContainer width="100%" height="100%">
        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={filteredData}>
          <PolarGrid />
          <PolarAngleAxis dataKey={xAxis} />
          <PolarRadiusAxis />
          <Radar
            name="Mike"
            dataKey={yAxis}
            stroke="#8884d8"
            fill="#8884d8"
            fillOpacity={0.6}
          />
        </RadarChart>
      </ResponsiveContainer>
    </div>
  );
}

export default SimpleRadar;
