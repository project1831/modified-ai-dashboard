import Head from "next/head";
import React, { useState, createContext, useContext } from "react";
import {
  PanelContent,
  WelcomeHeader,
  Dashboard,
  DataLoadedMessage,
  Button,
  Panel,
} from "../components";
import { generateDashboard } from "../openai";
import { IDashboard, IDataset, ISettings } from "../types";
import { parseData } from "../utils/parseData";
import PageLayout from "../components/layout/PageLayout";
import { useRouter } from "next/router";
import swal from "sweetalert";
import Chat from "../components/viz/Chat";
import { useCoreData } from "./_app";
import loader from "../Asset/b9c9c8e30af8371b8875cabb28849757.gif";
import Image from "next/image";

export default function Home() {
  const [settings, setSettings] = React.useState<ISettings>({
    url: "",
    auth: "",
    apikey: "",
    sampleRows: 1,
    model: "",
    mock: "",
    context: "",
    noOfGraph: 5,
    OPENAI_API_VERSION: "",
    OPENAI_API_BASE: "",
    deployment_name: "",
  });
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<IDataset>();
  const [userContext, setUserContext] = React.useState<string>("");
  const [dashboard, setDashboard] = React.useState<IDashboard | null>();
  let { coreData } = useCoreData();
  const [setKey, setNoKey] = useState(false);

  const router = useRouter();
  React.useEffect(() => {
    localStorage.setItem("chat_base_data", JSON.stringify([]));

    const config = localStorage.getItem("analyzer-settings");

    if (config) {
      setSettings(JSON.parse(config) as ISettings);
      setNoKey(true);
    }
    if (coreData) {
      setData(coreData);
    }

    setDashboard(null);
  }, []);

  const handleAnalyze = React.useCallback(() => {
    setLoading(true);
    const config = localStorage.getItem("analyzer-settings");
    const settingData = config && JSON.parse(config);
    if (!settingData?.apikey) {
      setNoKey(false);
      // swal("Warning!", "Please add API key", "info").then((value) => {
      //   router.push("/settings");
      // });
      setLoading(false);
    } else if (data) {
      setNoKey(true);

      generateDashboard(
        data,
        userContext,
        settings.sampleRows,
        settings.apikey,
        settings.model,
        settings.noOfGraph
      )
        .then((response) => {
          setDashboard(response.dashboard);
          // console.log("------response.dashboard--->", response.dashboard);
          setLoading(false);
        })
        .catch((err) => {
          console.log("------err--->", err);
          setDashboard(null);
          setLoading(false);
        });
    }
  }, [data, userContext, settings]);

  React.useEffect(() => {
    handleAnalyze();
  }, [settings]);

  const reAnalize = () => {
    localStorage.removeItem("gpt-data");
    handleAnalyze();
  };

  React.useEffect(() => {
    const handleBeforeUnload = (event: any) => {
      event.preventDefault();
      event.returnValue = ""; // Modern browsers require a returnValue to show the alert
    };

    const addBeforeUnloadListener = () => {
      window.addEventListener("beforeunload", handleBeforeUnload);
    };

    const removeBeforeUnloadListener = () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };

    addBeforeUnloadListener();

    return () => {
      removeBeforeUnloadListener();
    };
  }, []);

  return (
    <>
      <Head>
        <title>Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="author" content="dashboard" />
        {/* OG Meta tags */}
        <meta name="og:type" content="website" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <>
        <>
          <PageLayout
            Body={() => (
              <div style={{ overflowX: "hidden" }}>
                {!data ? (
                  <div className="center">
                    <div
                      className="center-button-wrapper"
                      onClick={() =>
                        setKey ? router.push("/data") : router.push("/settings")
                      }
                    >
                      <Button>
                        {setKey ? "Upload New Data" : "Add API Key"}
                      </Button>
                    </div>
                  </div>
                ) : (
                  <>
                    {loading ? (
                      <div className="center">
                        <div className="center-button-wrapper">
                          <center>
                            {/* <div className="loader"></div> */}
                            <Image
                              src={loader}
                              alt="ii"
                              style={{ height: 300, width: "100%" }}
                            />

                            <br />
                            <b style={{ color: "white" }}>Analyzing data...</b>
                          </center>
                        </div>
                      </div>
                    ) : (
                      <div style={{ position: "absolute" }}>
                        <div style={{ display: "flex", justifyContent: "end" }}>
                          <DataLoadedMessage onAnalyze={reAnalize} />
                          <Chat />
                        </div>
                        {!loading && dashboard && data ? (
                          <Dashboard data={data} dashboard={dashboard} />
                        ) : null}
                      </div>
                    )}
                  </>
                )}
              </div>
            )}
          />
        </>
      </>
    </>
  );
}
