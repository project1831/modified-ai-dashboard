import Head from "next/head";
import React from "react";
import {
  PanelContent,
  PanelHeader,
  WelcomeHeader,
  SettingsModal,
} from "../components";
import { Loader } from "../components/layout/Loader";
import { ISettings } from "../types";
import PageLayout from "../components/layout/PageLayout";
import Card from "@mui/material/Card";

import {
  AZURE_OPEN_AI_API,
  API_KEY,
  AUTH,
  OPENAI_API_BASE,
  OPENAI_API_VERSION,
  deployment_name,
} from "../config";

const defaultSettings = {
  url: AZURE_OPEN_AI_API,
  auth: AUTH,
  apikey: API_KEY,
  sampleRows: 1,
  model: "",
  mock: "false",
  context: "",
  noOfGraph: 4,
  OPENAI_API_VERSION: OPENAI_API_VERSION,
  OPENAI_API_BASE: OPENAI_API_BASE,
  deployment_name: deployment_name,
};

export default function Home() {
  const [settings, setSettings] = React.useState<ISettings>({
    url: "",
    auth: "",
    apikey: "",
    sampleRows: 1,
    model: "",
    mock: "true",
    context: "",
    noOfGraph: 5,
    OPENAI_API_VERSION: "",
    OPENAI_API_BASE: "",
    deployment_name: "",
  });

  React.useEffect(() => {
    const config = localStorage.getItem("analyzer-settings");

    if (config) {
      setSettings(JSON.parse(config) as ISettings);
    } else {
      localStorage.setItem(
        "analyzer-settings",
        JSON.stringify(defaultSettings)
      );
    }
  }, []);

  const handleSettingsChange = React.useCallback((settings: ISettings) => {
    localStorage.setItem("analyzer-settings", JSON.stringify(settings));
    setSettings(settings);
  }, []);

  const resetToDefault = () => {
    localStorage.setItem("analyzer-settings", JSON.stringify(defaultSettings));
    window.location.reload();
  };

  return (
    <>
      <Head>
        <title>Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="author" content="Leniolabs_ LLC" />
        {/* OG Meta tags */}
        <meta name="og:type" content="website" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>

      <PageLayout
        Body={() => (
          <div style={{ margin: 20 }}>
            <br />
            <Card style={{ padding: 30 }}>
              <SettingsModal
                value={settings}
                onChange={handleSettingsChange}
                resetToDefault={resetToDefault}
              />
            </Card>
          </div>
        )}
      />
    </>
  );
}
