import * as React from "react";
import GenerateDoc from "../components/viz/GenerateDoc";
import PageLayout from "../components/layout/PageLayout";
export default function manual() {
  return <PageLayout Body={() => <GenerateDoc />} />;
}
