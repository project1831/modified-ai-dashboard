import Head from "next/head";
import React from "react";
import {
  Button,
  ButtonsRow,
  Icon,
  PanelContent,
  PanelHeader,
  WelcomeHeader,
  ButtonIcon,
  TextInput,
  ButtonLink,
  UploadDatasetButton,
} from "../components";
import { Loader } from "../components/layout/Loader";
import { Table } from "../components/layout/Table";
import { IDataset } from "../types";
import { parseData, stringifyData } from "../utils/parseData";
import PageLayout from "../components/layout/PageLayout";
import DynamicDataTable from "../components/layout/Datatable";
import DataCleasingLogs from "../components/layout/DataCleasingLogs";
import { useCoreData, useChatData, useReportData } from "./_app";

import loader from "../Asset/digital-marketing-trends-2.gif";
import Image from "next/image";

export default function Home() {
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<IDataset>();
  const [edit, setEdit] = React.useState(false);
  const [logs, setLogs] = React.useState<any>([]);
  let { coreData, setCoreData } = useCoreData();
  let { reportData, setReportData } = useReportData();
  let { chatData, setChatData } = useChatData();

  React.useEffect(() => {
    setData(coreData);
  }, []);

  // console.log(dashboard, stringifyData(data || []));

  const handleClear = React.useCallback(() => {
    setData(undefined);
  }, []);

  const updateData = (dataset: string) => {
    setData(parseData(dataset));
    setCoreData(parseData(dataset));
  };
  const handleDatasetChange = React.useCallback((dataset: string) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
    updateData(dataset);
  }, []);

  const resetContext = () => {
    setReportData([]);
    setChatData([]);
    localStorage.removeItem("data-summary");
    localStorage.removeItem("gpt-data");
    localStorage.removeItem("report-data");
    console.log("Reset context");
  };

  React.useEffect(() => {
    const handleBeforeUnload = (event: any) => {
      event.preventDefault();
      event.returnValue = ""; // Modern browsers require a returnValue to show the alert
    };

    const addBeforeUnloadListener = () => {
      window.addEventListener("beforeunload", handleBeforeUnload);
    };

    const removeBeforeUnloadListener = () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };

    addBeforeUnloadListener();

    return () => {
      removeBeforeUnloadListener();
    };
  }, []);

  console.log("logs", logs);
  return (
    <>
      <Head>
        <title>Data</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="author" content="autograph" />
        {/* OG Meta tags */}
        <meta name="og:type" content="website" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <PageLayout
        Body={() => (
          <div style={{ margin: 20 }}>
            <>
              {loading && (
                <div className="center">
                  <div className="center-button-wrapper">
                    <center>
                      <Image
                        src={loader}
                        alt="ii"
                        style={{ height: 300, width: "100%" }}
                      />
                      <br />
                      <br />
                      <b style={{ color: "white" }}>
                        {" "}
                        Cleaning uploaded data...
                      </b>
                    </center>
                  </div>
                </div>
              )}

              <PanelHeader>
                <ButtonsRow>
                  <UploadDatasetButton
                    onUpload={handleDatasetChange}
                    resetContext={resetContext}
                    setLogs={setLogs}
                  />
                  <Button
                    className="trash"
                    disabled={!data}
                    outline
                    onClick={handleClear}
                  >
                    <Icon icon="thrash" /> Clear
                  </Button>
                  <Button
                    className="trash"
                    disabled={!data}
                    outline
                    onClick={() => setEdit(!edit)}
                    
                  >
                    <Icon icon="cog" /> {!edit ? "Edit Data" : "Done"}
                  </Button>
                </ButtonsRow>
              </PanelHeader>
              <br />

              <div>
                {" "}
                {logs && logs.length > 0 && <DataCleasingLogs logs={logs} />}
              </div>
              <br />
              <div style={{ display: "grid" }}>
                <div
                  style={{
                    // overflowY: "auto",
                    overflow: "auto",
                    position: "absolute",
                  }}
                >
                  {edit ? (
                    <Table
                      data={data || []}
                      onChange={(newData) => {
                        setCoreData(newData);
                        setData(newData);
                      }}
                    />
                  ) : (
                    <>
                      {data && (
                        <>
                          <DynamicDataTable data={data} />
                        </>
                      )}
                    </>
                  )}
                </div>
              </div>
            </>
          </div>
        )}
      />
    </>
  );
}
