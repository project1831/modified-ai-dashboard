import * as React from "react";
import GenerateReport from "../components/viz/GenerateReport";
import PageLayout from "../components/layout/PageLayout";
import Card from "@mui/material/Card";
import DocQNA from "../components/layout/DocQNA";

export default function manual() {
  return (
    <PageLayout
      Body={() => (
        <div>
          <DocQNA />
        </div>
      )}
    />
  );
}
