import "../styles/globals.css";
import type { AppProps } from "next/app";
import React, { useState, createContext, useContext, useEffect } from "react";

const CoreContext = createContext<any>(null);
const ChatContext = createContext<any>([]);
const ReportContext = createContext<any>([]);
const ConfigContext = createContext<any>([]);

export const useCoreData = () => useContext(CoreContext);
export const useChatData = () => useContext(ChatContext);
export const useReportData = () => useContext(ReportContext);
export const useConfig = () => useContext(ConfigContext);

export default function App({ Component, pageProps }: AppProps) {
  const [coreData, setCoreData] = useState(null);
  const [reportData, setReportData] = useState([]);
  const [chatData, setChatData] = useState([]);
  const [settings, setSettings] = React.useState<any>("");

  React.useEffect(() => {
    const getData = localStorage.getItem("analyzer-settings");
    const settingsData = getData && JSON.parse(getData);
    setSettings(settingsData);
  }, []);

  return (
    <ConfigContext.Provider value={{ settings }}>
      <CoreContext.Provider value={{ coreData, setCoreData }}>
        <ReportContext.Provider value={{ reportData, setReportData }}>
          <ChatContext.Provider value={{ chatData, setChatData }}>
            <div className="fading-background">
              <Component {...pageProps} />
            </div>
          </ChatContext.Provider>
        </ReportContext.Provider>
      </CoreContext.Provider>
    </ConfigContext.Provider>
  );
}
