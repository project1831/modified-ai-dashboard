export interface ISettings {
  auth: string
  url: string
  apikey: string
  sampleRows: number
  model: string
  mock: string
  context: string
  noOfGraph: number
  OPENAI_API_VERSION: string
  OPENAI_API_BASE: string
  deployment_name: string
}

export interface IFilter {
  title: string
  column: string
}

export interface IKPI {
  title: string
  javascriptFunction: string
}
export interface IChart {
  title: string
  chartType: string
  javascriptFunction: string
}

export interface IDashboard {
  filters: IFilter[]
  kpis: IKPI[]
  charts: IChart[]
  summary?: any
}

export type IDatasetRecord = {
  [key: string]: string
}

export type IDataset = IDatasetRecord[]

export type ChatInteraction = {
  question?: string
  reply?: string
}
