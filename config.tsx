export const OPEN_AI_COMPLETIONS_API =
  "https://api.openai.com/v1/chat/completions";
export const OPEN_AI_COMPLETIONS_DAVINCI_API =
  "https://api.openai.com/v1/completions";

export const AZURE_OPEN_AI_API =
  "https://soumenopenai.openai.azure.com/openai/deployments/gpt-35-turbo/chat/completions?api-version=2023-03-15-preview";

// export const Dashboard_bakeend_base_url = `https://ai-dashboar-backend.azurewebsites.net`;
export const Dashboard_bakeend_base_url = `http://127.0.0.1:5000`;

export const API_KEY = "3a5a6eba4d2546558d3fa749ef9fb5ce";
export const AUTH = "a8fe99cb6b354a06913f189536cdf8fc";
export const OPENAI_API_BASE = "https://soumenopenai.openai.azure.com";
export const OPENAI_API_VERSION = "2023-03-15-preview";
export const deployment_name = "gpt-35-turbo";
